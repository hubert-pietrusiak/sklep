const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/kiosk.js', 'public/js')
    .js('resources/js/orders.js', 'public/js')
    .js('resources/js/payoff.js', 'public/js')
    .js('resources/js/cockpit.js', 'public/js')
    .js('resources/js/products.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/product.scss', 'public/css')
    .sass('resources/sass/admin.scss', 'public/css')
    .version();