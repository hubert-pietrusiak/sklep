<?php

namespace App\Helpers;

use App\Order;
use Eloquent;
use function var_dump;

class StatHelper {
    static function dateFilter($model, $start_date, $end_date)
    {
        if($start_date && $end_date) {
            return $model::with('items')
                ->whereBetween('created_at', [$start_date, $end_date]);
        } else if($start_date) {
            return $model::with('items')->where('created_at', '>', $start_date);
        } else if($end_date) {
            return $model::with('items')->where('created_at', '<', $end_date);
        } else {
            return $model::with('items');
        }
    }

    static function getPayOff($start_date, $end_date, $user_id = null)
    {
        $sales = [];
        $purchases = [];
        $result = [];

        $orders = StatHelper::dateFilter(Order::class, $start_date, $end_date)->get();

        foreach ($orders as $order) {
            foreach ($order->items as $item) {
                if((int) $item->price !== 0) {
                    if(!empty($user_id) && $order->user_id != $user_id && $item->product->user_id != $user_id) {
                        continue;
                    }

                    if(!isset($purchases[$order->user_id])) {
                        $purchases[$order->user_id] = 0;
                    }
                    $purchases[$order->user_id] += (int) ($item->price * $item->quantity);

                    if(!isset($sales[$item->product->user_id])) {
                        $sales[$item->product->user_id] = 0;
                    }
                    $sales[$item->product->user_id] += (int) ($item->price * $item->quantity);
                }
            }
        }


        collect($purchases)->each(function($item, $key) use (&$result) {
            if(!isset($result[$key])) {
                $result[$key] = [];
            }

            $result[$key]['purchases'] = $item;
        });

        collect($sales)->each(function($item, $key) use (&$result) {
            if(!isset($result[$key])) {
                $result[$key] = [];
            }

            $result[$key]['sales'] = $item;
        });

        $collection = collect($result)
            ->map(function($item, $key){
                $purchasesValue = isset($item['purchases']) ? $item['purchases'] : 0;
                $salesValue = isset($item['sales']) ? $item['sales'] : 0;

                return [
                    'user_id' => $key,
                    'purchases' => $purchasesValue,
                    'sales' => $salesValue,
                    'balance' => $purchasesValue - $salesValue
                ];
            });


        if(!empty($user_id)) {
            return $collection->where('user_id', $user_id)->first();
        } else {
            return array_values($collection->values()->all());
        }
    }

    static function getGeneralStats()
    {

    }

    static function getOrderHistory($start_date, $end_date, $user_id = null)
    {

    }
}