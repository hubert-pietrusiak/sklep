<?php

namespace App\SlashCommandHandlers;

use App\Helpers\SlackHelper;
use App\Helpers\StatHelper;
use App\User;
use Carbon\Carbon;
use function sizeof;
use Spatie\SlashCommand\Attachment;
use Spatie\SlashCommand\AttachmentAction;
use Spatie\SlashCommand\Handlers\BaseHandler;
use Spatie\SlashCommand\Request;
use Spatie\SlashCommand\Response;
use Spatie\SlashCommand\Handlers\SignatureHandler;
use function strlen;
use function strpos;

class Balance extends BaseHandler
{
    /**
     * Handle the given request. Remember that Slack expects a response
     * within three seconds after the slash command was issued. If
     * there is more time needed, dispatch a job.
     *
     * @param \Spatie\SlashCommand\Request $request
     *
     * @return \Spatie\SlashCommand\Response
     */
    public function handle(Request $request): Response
    {
        $userId = $request->userId;
        $user  = User::where('slack_userid', $userId)->first();

        $input_array = explode(" ", $request->text ?? "");

        $input = [
          'month' => isset($input_array[1]) ? SlackHelper::month($input_array[1]) : null,
          'year' =>  isset($input_array[2]) ? $input_array[2] : null
        ];

        if(!$user) {
            $slack_name = $request->userName;
            return $this->respondToSlack("Hej @${slack_name}! Nie ma Cię w bazie sklepu :( Skontaktuj się z administratorem. Czy coś.");
        } else {
            $name = explode(" ", $user->name)[0];

            if(!empty($input['month'])) {
                if(!empty($input['year']) && strlen($input['year']) === 4 && ctype_digit($input['year'])) {
                    $start_date_str = SlackHelper::message('carbon.first_day', [':month' => $input['month'], ':year' => $input['year']]);
                    $end_date_str = SlackHelper::message('carbon.last_day', [':month' => $input['month'], ':year' => $input['year']]);
                    $balance_message_id = 'balance.specific_month_and_year';
                } else {
                    $start_date_str = SlackHelper::message('carbon.first_day', [':month' => $input['month'], ':year' => '']);
                    $end_date_str = SlackHelper::message('carbon.last_day', [':month' => $input['month'], ':year' => '']);
                    $balance_message_id = 'balance.specific_month';
                }
            } else if(isset($input_array[1]) && ($input_array[1] == 'ostatni' || $input_array[1] == 'poprzedni')) {
                $start_date_str = SlackHelper::message('carbon.first_day', [':month' => 'last month', ':year' => '']);
                $end_date_str = SlackHelper::message('carbon.last_day', [':month' => 'last month', ':year' => '']);
                $balance_message_id = 'balance.last_month';
            } else {
                $start_date_str = SlackHelper::message('carbon.first_day', [':month' => 'this month', ':year' => '']);
                $end_date_str = SlackHelper::message('carbon.last_day', [':month' => 'this month', ':year' => '']);
                $balance_message_id = 'balance.current_month';
            }

            $start_date = new Carbon($start_date_str);
            $end_date = new Carbon($end_date_str);

            $payoff = $user->payOff($start_date, $end_date);

            if(!empty($payoff)) {
                $balance = $payoff['balance'] / 100;
            } else {
                $balance = 0.00;
            }

            $balance = number_format($balance,2,",",".");

            $message = SlackHelper::message($balance_message_id, [':name' => $name, ':balance' => $balance, ':month' => isset($input_array[1]) ? $input_array[1] :'', ':year' => $input['year']]);

            $message .= "\n\n".SlackHelper::message('balance.payoff_end_of_month');

            if($balance < 0) {
                $message .= "\n".SlackHelper::message('balance.negative_meaning');
            }

            if(empty($request->text)) {
                $message .= "\n\n".SlackHelper::message('command.list');
            }

            return $this->respondToSlack($message);
        }
    }

    /**
     * If this function returns true, the handle method will get called.
     *
     * @param \Spatie\SlashCommand\Request $request
     *
     * @return bool
     */
    public function canHandle(Request $request): bool
    {
        return empty($request->text) || strpos($request->text, 'bilans') !== false;
    }
}