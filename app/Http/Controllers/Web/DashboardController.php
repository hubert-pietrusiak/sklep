<?php

namespace App\Http\Controllers\Web;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function cockpit()
    {
        return view('admin.cockpit');
    }

    public function orders()
    {
        return view('admin.orders');
    }

    public function payoff()
    {
        return view('admin.payoff');
    }

    public function products()
    {
        return view('admin.products');
    }

    public function printProduct(Product $product)
    {
        return view('product')->with('product', $product);
    }

    public function users()
    {
        return view('admin.users');
    }
}
