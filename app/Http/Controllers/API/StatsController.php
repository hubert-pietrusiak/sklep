<?php

namespace App\Http\Controllers\API;

use App\Order;
use App\OrderItem;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StatsController extends Controller
{
    public function getStats(Request $request)
    {
        $last_order = Order::latest()
            ->with(['items', 'user'])
            ->get()
            ->first();

        $most_common_purchase = OrderItem::with('product')
            ->groupBy('product_id')
            ->orderByRaw('COUNT(*) DESC')
            ->limit(1)
            ->get()
            ->pluck('product');

        $current_products_value = Product::all()
            ->reduce(function ($carry, $item) {
                return $carry + (($item->quantity > 0 ? $item->quantity : 0) * $item->price);
            }, 0);

        $total_sales_value = Order::with('items')
            ->get()
            ->reduce(function ($carry, $item) {
                $sum = $item->items->reduce(function($carryInner, $itemInner){
                    return $carryInner + ($itemInner->quantity * $itemInner->price);
                });
                return $carry + $sum;
            }, 0);

        return [
            'last_order' => $last_order,
            'most_common_purchase' => $most_common_purchase[0],
            'current_products_value' => $current_products_value,
            'total_sales_value' => $total_sales_value
        ];
    }
}
