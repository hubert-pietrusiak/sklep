<?php

namespace App\Http\Controllers\API;

use App\Events\Products\ProductCreated;
use App\Events\Products\ProductDeleted;
use App\Events\Products\ProductEdited;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Product[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(Request $request)
    {
        $products = Product::with('user');

        if(!$request->has('with_zero_quantity')) {
            $products = $products->where('quantity', '<>', 0);
        }
        return $products->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = Product::create($request->all());
        event(new ProductCreated($product));
        return $product;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product $product
     * @return Product
     */
    public function show(Product $product)
    {
        return $product;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return Product
     */
    public function update(Request $request, Product $product)
    {
        $product->update($request->all());
        event(new ProductEdited($product));
        return $product;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        event(new ProductDeleted($product));
        return $product;
    }
}
