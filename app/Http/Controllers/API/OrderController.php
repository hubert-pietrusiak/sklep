<?php

namespace App\Http\Controllers\API;

use App\Events\Orders\OrderCreated;
use App\Order;
use App\OrderItem;
use App\OrderRepository;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use function var_dump;

class OrderController extends Controller
{

    private $repository;

    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return OrderRepository|\Illuminate\Http\Response
     */
    public function index()
    {
        return $this->repository->with(['items', 'user'])->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(Request $request)
    {
        $order = $this->repository->create([
            'user_id' => $request->user_id,
            'items' => $request->items
        ]);

        event(new OrderCreated($order));

        return $order;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  \App\Order $order
     * @return Order|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Request $request, Order $order)
    {
        if($request->revert_quantity) {
            foreach ($order->items()->get() as $item)
            {
                $item->product->quantity += $item->quantity;
                $item->product->save();
            }
        }

        $order->delete();

        return $order;
    }
}
