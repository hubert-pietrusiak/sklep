<?php

namespace App\Http\Controllers\API;

use App\Helpers\StatHelper;
use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PayoffController extends Controller
{
    public function getPayoff(Request $request)
    {
        return StatHelper::getPayOff($request->start_date, $request->end_date);
    }
}
