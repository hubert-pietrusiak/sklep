<?php

namespace App;

use Prettus\Repository\Eloquent\BaseRepository;

class ProductRepository extends BaseRepository {
    /**
     * @return string
     */
    function model()
    {
        return 'App\Product';
    }
}
