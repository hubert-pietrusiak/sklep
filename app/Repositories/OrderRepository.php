<?php

namespace App;

use Prettus\Repository\Eloquent\BaseRepository;

class OrderRepository extends BaseRepository {

    protected $fieldSearchable = [
        'id',
        'user_id',
        'items.product.id',
        'items.product.user_id',
        'items.product.name' => 'like',
        'user.id',
        'user.name' => 'like',
        'user.tile_name',
        'user.username' => 'like'
    ];
    /**
     * @return string
     */
    function model()
    {
        return 'App\Order';
    }

    public function create(array $attributes)
    {
        $order = Order::create([
            "user_id" => $attributes['user_id']
        ]);

        foreach ($attributes['items'] as $item)
        {
            $product = Product::find($item['id']);
            $price = ($attributes['user_id'] == $product->user_id) ? 0 : (int) $product->price;

            OrderItem::create([
                "product_id" => $item['id'],
                "order_id" => $order->id,
                "quantity" => $item['quantity'],
                "price" => $price
            ]);

            $product->quantity -= (int) $item['quantity'];
            $product->save();
        }

        return $order;
    }

    public function boot(){
        $this->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
    }
}
