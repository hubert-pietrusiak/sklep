<?php

namespace App\Notifications;

use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use function rtrim;

class OrderCreatedNotification extends Notification implements ShouldQueue
{
    use Queueable;
    public $order;

    /**
     * Create a new notification instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable)
    {
        $purchaser_name = $this->order->user->name;

        $itemsFormatted = $this->order->items->reduce(function($memo, $element){
            return $memo.($element->quantity.'x '.$element->product->name).',';
        }, '');
        $itemsFormatted = rtrim($itemsFormatted, ',');

        $gross = $this->order->items->reduce(function($memo, $element){
            return $memo + ((int)($element->price) * $element->quantity);
        }, 0);
        $gross = number_format($gross / 100,2,",",".");


        return (new SlackMessage)
            ->from('Vestiasklep', ':shopping_bags:')
            ->to(env('SLACK_NOTIFICATIONS_CHANNEL'))
            ->content("${purchaser_name} przed chwilą kupił(a) ${itemsFormatted}. Suma zamówienia: ${gross}zł");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
