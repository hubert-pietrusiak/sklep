<?php
namespace App;

use Illuminate\Notifications\Notifiable;

class Admin {
    use Notifiable;

    public function routeNotificationForSlack()
    {
        return env('SLACK_NOTIFICATIONS_WEBHOOK');
    }
}