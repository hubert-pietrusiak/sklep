<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\Products\ProductCreated' => [],
        'App\Events\Products\ProductDeleted' => [],
        'App\Events\Products\ProductEdited' => [],
        'App\Events\Frontend\RefreshTerminal' => [],
        'App\Events\Orders\OrderCreated' => [
            'App\Listeners\Orders\SendSlackOrderNotification',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
