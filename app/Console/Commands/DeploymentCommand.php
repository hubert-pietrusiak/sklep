<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DeploymentCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deploy:default';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run deployment script at default server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \SSH::run([
            'cd /home/ubuntu/sklep',
            './deploy.sh',
        ]);
    }
}
