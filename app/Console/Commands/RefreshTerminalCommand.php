<?php

namespace App\Console\Commands;

use App\Events\Frontend\RefreshTerminal;
use Illuminate\Console\Command;

class RefreshTerminalCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'frontend:refresh-terminal';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh (reload) page on every terminal';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        event(new RefreshTerminal());
    }
}
