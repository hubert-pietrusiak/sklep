<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/admin');

Route::get('/terminal', function () {
    return view('kiosk');
});

Route::prefix('admin')->middleware('auth.basic')->name('admin.')->group(function() {
    Route::get('', 'Web\DashboardController@cockpit')->name('cockpit');
    Route::get('orders', 'Web\DashboardController@orders')->name('orders');
    Route::get('payoff', 'Web\DashboardController@payoff')->name('payoff');
    Route::get('products', 'Web\DashboardController@products')->name('products');
    Route::get('products/{product}/print', 'Web\DashboardController@printProduct')->name('printProduct');
    Route::get('users', 'Web\DashboardController@users')->name('users');
});
