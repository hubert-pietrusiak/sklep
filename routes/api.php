<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('users', 'API\UserController');
Route::apiResource('products', 'API\ProductController');
Route::apiResource('orders', 'API\OrderController');

Route::get('payoff', 'API\PayoffController@getPayoff');
Route::get('stats', 'API\StatsController@getStats');
