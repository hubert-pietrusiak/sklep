echo "Killing queue and event processes"
pkill -f "php artisan queue:work"
laravel-echo-server stop
pkill -f laravel-echo-server

echo "Updating code"
git pull origin --rebase

echo "Running `composer install`"
composer install

echo "Migrating DB"
php artisan migrate

echo "Starting queue and event processes"
nohup php artisan queue:work --daemon > /dev/null 2>&1 &
nohup laravel-echo-server start > /dev/null 2>&1 &

echo "Waiting 5 seconds for terminals to refresh"
sleep 5
php artisan frontend:refresh-terminal

echo "Done"