<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Cache-control" content="no-cache">
    <meta http-equiv="Expires" content="-1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://unpkg.com/vue2-animate/dist/vue2-animate.min.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="{{ mix('css/admin.css') }}" rel="stylesheet">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:200,600" rel="stylesheet" type="text/css">
</head>
<body>
<div class="wrapper">

    <nav id="sidebar">
        <div class="sidebar-header">
            <h3>
                <span>Panel</span> Administracyjny
            </h3>
        </div>

        <ul class="list-unstyled components">
            <li class="{{ active('admin.cockpit') }}">
                <a href="{{ route('admin.cockpit') }}">Kokpit</a>
            </li>
            <li class="{{ active('admin.orders') }}">
                <a href="{{ route('admin.orders') }}">Zakupy</a>
            </li>
            <li class="{{ active('admin.products') }}">
                <a href="{{ route('admin.products') }}">Produkty</a>
            </li>
            <li class="{{ active('admin.payoff') }}">
                <a href="{{ route('admin.payoff') }}">Rozliczenie</a>
            </li>
            <li class="{{ active('admin.users') }}">
                <a href="{{ route('admin.users') }}">Użytkownicy</a>
            </li>
        </ul>
    </nav>

    <!-- Page Content -->
    <div id="content">
        @yield('content')
        <div id="app"></div>
    </div>

</div>
</body>
@yield('script')
</html>
