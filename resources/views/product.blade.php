<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="{{ mix('css/product.css') }}" rel="stylesheet">

    <title>Product page</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:200,600" rel="stylesheet" type="text/css">
    <script src="https://cdn.jsdelivr.net/npm/fitty@2.2.6/dist/fitty.min.js"></script>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="product-container">
        <div class="price-container" style="width: 150px">
            <div class="price">
                {{number_format($product->price / 100,2,",",".")}}
            </div>
        </div>
        <div class="title-container">
            <h1 id="title">{{$product->name}}</h1>
        </div>
        <span>Dostarczone przez: <strong>{{$product->user->name}}</strong></span>
    </div>
</div>
</body>
<script>
    var css = '@page { size: landscape; }',
        head = document.head || document.getElementsByTagName('head')[0],
        style = document.createElement('style');

    style.type = 'text/css';
    style.media = 'print';

    if (style.styleSheet){
        style.styleSheet.cssText = css;
    } else {
        style.appendChild(document.createTextNode(css));
    }

    head.appendChild(style);
    fitty("#title", {
        maxSize: 120
    });

    fitty(".price", {
        maxSize: 60
    });

    window.print();
</script>
</html>
