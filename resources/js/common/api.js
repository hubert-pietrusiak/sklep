import axios from 'axios';

export default {
    getProducts(data) {
        return axios.get('/api/products', {params: data});
    },
    createProduct(data) {
        return axios.post('/api/products', data);
    },
    updateProduct(id, data) {
        return axios.patch(`/api/products/${id}`, data);
    },
    deleteProduct(id) {
        return axios.delete(`/api/products/${id}`);
    },
    getUsers() {
        return axios.get('/api/users');
    },
    getOrders(data) {
        return axios.get('/api/orders', {params: data});
    },
    createOrder(data) {
        return axios.post('/api/orders', data);
    },
    deleteOrder(data) {
        return axios.delete(`/api/orders/${data.id}?revert_quantity=${data.revert_quantity || false}`);
    },
    getPayoff(data) {
        return axios.get('/api/payoff', {params: data});
    },
    getStats() {
        return axios.get('/api/stats');
    }
}
