export default {
    price(value) {
        return (value / 100).toFixed(2).replace('.', ',');
    },
    items(items) {
        let resultString = '';

        for(let i = 0; i < items.length; i++) {
            const item = items[i];
            resultString += `${item.quantity}x ${item.product.name}`;

            if(i < items.length - 1) {
                resultString += ', ';
            }
        }
        return resultString;
    }
}