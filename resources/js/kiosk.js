
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./common/bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

Vue.component('clock-component', require('./components/ClockComponent'));
Vue.component('kiosk-component', require('./components/KioskComponent.vue'));
Vue.component('product-item', require('./components/ProductItem'));
Vue.component('product-list', require('./components/ProductList'));
Vue.component('details-component', require('./components/DetailsComponent.vue'));
Vue.component('initials-component', require('./components/InitialsComponent.vue'));
Vue.component('basket-list', require('./components/BasketList.vue'));
Vue.component('user-list', require('./components/UserList.vue'));
Vue.component('user-item', require('./components/UserItem'));
Vue.component('bottom-bar', require('./components/BottomBar'));
Vue.component('summary-component', require('./components/SummaryComponent'));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    template: `<kiosk-component></kiosk-component>`
});
