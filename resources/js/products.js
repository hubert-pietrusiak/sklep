
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import BootstrapVue from 'bootstrap-vue'

require('./common/bootstrap');

window.Vue = require('vue');

Vue.use(BootstrapVue);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
Vue.component('user-list', require('./components/UserList.vue'));
Vue.component('user-item', require('./components/UserItem'));
Vue.component('product-item', require('./components/ProductItem'));
Vue.component('dashboard-products-component', require('./components/DashboardProductsComponent'));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    template: `<dashboard-products-component></dashboard-products-component>`
});
